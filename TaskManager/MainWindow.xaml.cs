﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TaskManager.Class;

namespace TaskManager
{
    public partial class MainWindow : Window
    {
        private ConnectionDataBase conn = ConnectionDataBase.creatObj();
        private int currentID = 0; // Selected id (to edit)

        public MainWindow()
        {
            InitializeComponent();

        }
        // Load tasks on start
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.updateDataGrid();
        }

        // Show Tasks, Update DataGrid
        private void updateDataGrid()
        {
            DataTable dt = conn.SelectAllTasks();
            myDataGrid.ItemsSource = dt.DefaultView;
        }

        private void Add_btn_Click(object sender, RoutedEventArgs e)
        {
            DateTime? date = myDatapicker.SelectedDate; // no date selected, Nullable
            conn.AddTask(Text_box_text.Text, Piority_comboBox.Text, Staus_comboBox.Text, date);
            updateDataGrid(); 
            ResetUI();
        }

        private void Update_btn_Click(object sender, RoutedEventArgs e)
        {
            DateTime? date = myDatapicker.SelectedDate; // no date selected, Nullable
            conn.UpdateTask(Text_box_text.Text, Piority_comboBox.Text, Staus_comboBox.Text, date, currentID);
            updateDataGrid();
            ResetUI();
        }

        private void Reset_btn_Click(object sender, RoutedEventArgs e)
        {
            ResetUI();
        }

        private void Delete_btn_Click(object sender, RoutedEventArgs e)
        {
            conn.DeleteTask(currentID);
            updateDataGrid();
            ResetUI();
        }

        // Select task to edit
        private void MyDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;
            DataRowView dataRowView = dataGrid.SelectedItem as DataRowView;
            if (dataRowView != null)
            {
                Text_box_text.Text = dataRowView["Tekst"].ToString();
                Piority_comboBox.Text = dataRowView["Piority"].ToString();
                if (dataRowView["Date"].ToString().Length > 0) // Task don't have date
                {
                    myDatapicker.SelectedDate = DateTime.Parse(dataRowView["Date"].ToString());
                }
                else
                {
                    myDatapicker.SelectedDate = null;
                }
                Staus_comboBox.Text = dataRowView["Status"].ToString();
                currentID = Int32.Parse(dataRowView["id"].ToString());
            }
            add_btn.IsEnabled = false;
            update_btn.IsEnabled = true;
            delete_btn.IsEnabled = true;
        }

        // Clear Task UI
        private void ResetUI()
        {
            Text_box_text.Text = "";
            myDatapicker.SelectedDate = null;
            Piority_comboBox.SelectedIndex = 1;
            Staus_comboBox.SelectedIndex = 0;
            myDataGrid.UnselectAll();
            add_btn.IsEnabled = true;
            update_btn.IsEnabled = false;
            delete_btn.IsEnabled = false;
        }



    }
}
