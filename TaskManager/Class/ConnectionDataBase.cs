﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TaskManager.Class
{
    class ConnectionDataBase
    {
        private readonly string connectionString = TaskManager.Properties.Settings.Default.ConnectionString; // Connection String

        // Singleton
        private static ConnectionDataBase instance;
        private ConnectionDataBase() { }
        public static ConnectionDataBase creatObj()
        {
            if (instance == null)
            {
                instance = new ConnectionDataBase();
            }
            return instance;
        }

        // Select, get all tasks
        public DataTable SelectAllTasks()
        {
            DataTable dataTable = new DataTable();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT * FROM [Tasks]";
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        dataTable.Load(reader);
                        reader.Close();
                        connection.Close();
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine(e.ToString(), "Error Message");
                    }
                }
            }
            return dataTable;
        }

        public void AddTask(string tekst, string piority, string status, DateTime? date)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    SqlParameter param1 = new SqlParameter("@Text", tekst);
                    SqlParameter param2 = new SqlParameter("@Piority", piority);
                    SqlParameter param3 = new SqlParameter("@Date", (date == null) ? (object)DBNull.Value : date); // no date selected, null parametr
                    SqlParameter param4 = new SqlParameter("@Stauts", status);
                    command.CommandText = @"INSERT INTO [Tasks] (Tekst, Piority, Date, Status) VALUES (@Text,@Piority,@Date,@Stauts)";
                    command.Parameters.Add(param1);
                    command.Parameters.Add(param2);
                    command.Parameters.Add(param3);
                    command.Parameters.Add(param4);
                    Execute(connection, command, "Zadanie dodane");
                }
            }
        }

        public void UpdateTask(string tekst, string piority, string status, DateTime? date, int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    SqlParameter param1 = new SqlParameter("@Text", tekst);
                    SqlParameter param2 = new SqlParameter("@Piority", piority);
                    SqlParameter param3 = new SqlParameter("@Date", (date == null) ? (object)DBNull.Value : date); // no date selected, null parametr
                    SqlParameter param4 = new SqlParameter("@Stauts", status);
                    SqlParameter param5 = new SqlParameter("@id", id);
                    command.CommandText = @"UPDATE [Tasks] SET Tekst = @Text, Piority = @Piority, Date = @Date, Status = @Stauts WHERE id = @Id";
                    command.Parameters.Add(param1);
                    command.Parameters.Add(param2);
                    command.Parameters.Add(param3);
                    command.Parameters.Add(param4);
                    command.Parameters.Add(param5);
                    Execute(connection, command, "Pomyślna edycja zadania");
                }
            }
        }

        public void DeleteTask(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    SqlParameter param1 = new SqlParameter("@id", id);
                    command.CommandText = @"DELETE FROM [Tasks] WHERE id = @ID";
                    command.Parameters.Add(param1);
                    Execute(connection, command, "Zadanie usinięto");
                }
            }
        }

        // open/close connection, execute Query
        public void Execute(SqlConnection connection, SqlCommand command, string msg)
        {
            try
            {
                connection.Open();
                int n = command.ExecuteNonQuery();
                if (n > 0)
                {
                    MessageBox.Show(msg);
                }
                else
                {
                    MessageBox.Show("Nie udało się");
                }
                connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString(), "Error Message");
            }
        }
    }
}
